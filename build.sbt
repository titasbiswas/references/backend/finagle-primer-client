name := "finagle-primer-client"

version := "0.1"

scalaVersion := "2.13.3"

libraryDependencies += "log4j" % "log4j" % "1.2.17"

//finagle
libraryDependencies += "com.twitter" %% "finagle-http" % "20.10.0"