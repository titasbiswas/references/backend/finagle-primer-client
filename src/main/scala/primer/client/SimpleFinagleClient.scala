package primer.client

import java.net.InetSocketAddress
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.twitter.finagle.Http
import com.twitter.finagle.builder.ClientBuilder
import com.twitter.finagle.http._


object SimpleFinagleClient extends App {

  val address = new InetSocketAddress(8180)
  val dtf = DateTimeFormatter.ofPattern("hh:mm:ss")

  val client = ClientBuilder()
    .stack(Http.client)
    .hosts(address)
    .hostConnectionLimit(1)
    .build()

  val req =  Request(Version.Http11, Method.Get, "/greet")
  req.setContentString("Sikander")
  val resp = client(req)
  println(s"Call returned from service at ${dtf.format(LocalDateTime.now)}")

  resp onSuccess(cont=> println(s"Recieved the content ${cont.contentString} at  ${dtf.format(LocalDateTime.now)}"))
  resp onFailure(ex=> print("Exception occured", ex))
}
